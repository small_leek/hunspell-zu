Name:                hunspell-zu
Version:             0.20100126
Release:             18
Summary:             Zulu hunspell dictionaries
License:             GPLv3+
URL:                 https://extensions.openoffice.org/en/project/zulu-spell-checker
Source:              https://downloads.sourceforge.net/project/aoo-extensions/3132/3/dict-zu_za-2010.01.26.oxt
BuildArch:           noarch
Requires:            hunspell
Supplements:         (hunspell and langpacks-zu)

%description
Zulu hunspell dictionaries.

%prep
%autosetup -c -p1

%build
if ! iconv -f utf-8 -t utf-8 -o /dev/null README-zu_ZA.txt > /dev/null 2>&1; then
  iconv -f ISO-8859-2 -t UTF-8 README-zu_ZA.txt > README-zu_ZA.txt.new
  touch -r README-zu_ZA.txt README-zu_ZA.txt.new
  mv -f README-zu_ZA.txt.new README-zu_ZA.txt
fi
tr -d '\r' < README-zu_ZA.txt > README-zu_ZA.txt.new
touch -r README-zu_ZA.txt README-zu_ZA.txt.new
mv -f README-zu_ZA.txt.new README-zu_ZA.txt

%install
install -Dp zu_ZA.aff $RPM_BUILD_ROOT/%{_datadir}/myspell/zu.aff
cp -p zu_ZA.dic $RPM_BUILD_ROOT/%{_datadir}/myspell/zu.dic

%files
%doc README-zu_ZA.txt
%{_datadir}/myspell/*

%changelog
* Mon Jul 6 2020 yanan li <liyanan032@huawei.com> - 0.20100126-18
- Package init
